from typing import Generic, TypeVar, cast

import strawberry
from typing import Annotated
import strawberry
from settings import DEFAULT_LIMIT
from src.context import Context
from src.books.models import (
    CreateBookInput,
    Book,
    create_book,
    get_books,
)
from strawberry.types import Info

from src.users.gql import UserType


@strawberry.type
class PageInfo:
    has_next_page: bool
    has_previous_page: bool
    start_cursor: str | None
    end_cursor: str | None


GenericType = TypeVar('GenericType')


@strawberry.type
class Connection(Generic[GenericType]):
    page_info: PageInfo
    edges: list['Edge[GenericType]']
    nodes: list['GenericType']


@strawberry.type
class Edge(Generic[GenericType]):
    node: GenericType
    cursor: str


@strawberry.experimental.pydantic.type(model=Book, name='Book')
class BookType():
    id: strawberry.auto
    title: strawberry.auto

    @strawberry.field
    async def user(
        self,
        root: Book,
        info: Info[Context, None],
    ) -> UserType:
        user = await info.context.user_loader.load(
            key=root.user_id,
        )
        if not user:
            raise ValueError('user not found')
        return user


@strawberry.type(name='Query', extend=True)
class BookQuery:

    @strawberry.field(name='books')
    async def books(
        self,
        info: Info[Context, None],
        after: str | None = None,
        first: int | None = DEFAULT_LIMIT,
    ) -> Connection[BookType]:
        first = (first if first else DEFAULT_LIMIT) + 1

        edges = [
            Edge(
                cursor=str(book.id),
                node=book,
            )
            for book in cast(
                list[BookType],
                await get_books(
                    db=info.context.db,
                    after=after,
                    first=first,
                ),
            )
        ]
        has_next_page = len(edges) == first
        edges = edges[:-1]

        return Connection(
            page_info=PageInfo(
                has_next_page=has_next_page,
                has_previous_page=after is not None,
                start_cursor=edges[0].cursor if edges else None,
                end_cursor=edges[-1].cursor if edges else None,
            ),
            edges=edges,
            nodes=[edge.node for edge in edges],
        )


@strawberry.experimental.pydantic.input(
    model=CreateBookInput, name='CreateBookInput',
)
class CreateBookInputType:
    title: strawberry.auto
    user_id: strawberry.auto


@strawberry.type(name='Mutation', extend=True)
class BookMutation:

    @strawberry.mutation()
    async def create_book(
        self,
        info: Info[Context, None],
        create_input_gql: Annotated[
            CreateBookInputType,
            strawberry.argument(name='input')
        ],
    ) -> BookType:
        create_input = create_input_gql.to_pydantic()
        return cast(
            BookType,
            await create_book(
                db=info.context.db,
                create_input=create_input,
            ),
        )
