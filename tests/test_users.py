import pytest
from tests.fixtures.clients import GraphQLTestClient

pytestmark = [
    pytest.mark.asyncio,
]


create_user_query = """
    mutation createUser(
        $input: CreateUserInput!
    ) {
        createUser(input: $input) {
            id
        }
    }
"""


async def test_create_user(
    client: GraphQLTestClient,
    mocker,
):
    resp = await client.query(
        query=create_user_query,
        variables={
            'input': {
                'name': 'John Doe',
            },
        },
    )
    assert resp.data is not None
    assert resp.data['createUser'] == {'id': mocker.ANY}
