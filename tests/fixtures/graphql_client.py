import json
import typing as tp

import databases
from httpx import AsyncClient
from httpx._types import HeaderTypes, RequestFiles
from strawberry.test import BaseGraphQLTestClient, Response


class GraphQLTestClient(BaseGraphQLTestClient):

    def __init__(self, client: AsyncClient, db: databases.Database):
        self._client = client
        self.db = db

    async def query(
        self,
        query: str,
        variables: tp.Optional[tp.Dict[str, tp.Any]] = None,
        headers: tp.Optional[tp.Dict[str, object]] = None,
        asserts_errors: tp.Optional[bool] = True,
        files: tp.Optional[tp.Dict[str, object]] = None,
    ) -> Response:
        """Modifying query to return only sync."""
        body = self._build_body(query, variables, files)

        resp = await self.request(body, headers, files)
        raw_data = self._decode(resp, type='multipart' if files else 'json')

        response = Response(
            errors=raw_data.get('errors'),
            data=raw_data.get('data'),
            extensions=raw_data.get('extensions'),
        )
        if asserts_errors:
            assert response.errors is None, response.errors

        return response

    async def request(
        self,
        body: dict[str, object],
        headers: tp.Optional[dict[str, object]] = None,
        files: tp.Optional[dict[str, object]] = None,
    ):
        """Implement actual request."""
        return await self._client.post(
            '/graphql/',
            json=None if files else body,
            data=body if files else None,
            files=tp.cast(tp.Optional[RequestFiles], files),
            headers=tp.cast(tp.Optional[HeaderTypes], files),
            follow_redirects=True,
        )

    def _build_body(
        self,
        query: str,
        variables: tp.Optional[dict[str, tp.Mapping]] = None,  # type:ignore
        files: tp.Optional[dict[str, object]] = None,
    ) -> dict[str, object]:
        """Build body to ignore files."""

        body: dict[str, object] = {'query': query}

        if variables:
            body['variables'] = variables

        if files:
            assert variables is not None
            assert files is not None
            file_map = self._build_multipart_file_map(variables, files)

            body = {
                'operations': json.dumps(body),
                'map': json.dumps(file_map),
            }
        return body

    def _decode(
        self,
        response,
        type: tp.Literal['multipart', 'json'],
    ):
        """Always decode to json."""
        return response.json()
