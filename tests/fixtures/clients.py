import asyncio

import databases
import pytest
import pytest_asyncio
import settings
from httpx import AsyncClient
from run import get_app
from tests.fixtures.graphql_client import GraphQLTestClient
from yoyo import get_backend, read_migrations

TEST_DB_NAME = 'test_{name}'.format(name=settings.DB_NAME)


@pytest.fixture(scope='session')
def event_loop():
    return asyncio.new_event_loop()


@pytest_asyncio.fixture(autouse=True, scope='session')
async def prepare_db():
    postgres_db = databases.Database(
        settings.CONN_TEMPLATE.format(
            user=settings.DB_USER,
            password=settings.DB_PASSWORD,
            port=settings.DB_PORT,
            host=settings.DB_SERVER,
            name='postgres',
        ),
    )
    async with postgres_db as create_conn:
        await create_conn.execute(
            'drop database if exists {name};'.format(name=TEST_DB_NAME),
        )
        await create_conn.execute(
            'drop database if exists {name}_template;'.format(
                name=TEST_DB_NAME,
            ),
        )
        await create_conn.execute(
            'create database {name};'.format(name=TEST_DB_NAME),
        )

    backend = get_backend(
        settings.MIGRATIONS_CONN_TEMPLATE.format(
            user=settings.DB_USER,
            password=settings.DB_PASSWORD,
            port=settings.DB_PORT,
            host=settings.DB_SERVER,
            name=TEST_DB_NAME,
        ),
    )
    migrations = read_migrations('./migrations')
    with backend.lock():
        backend.apply_migrations(backend.to_apply(migrations))
    del backend  # connection is not released otherwise

    async with postgres_db as template_conn:
        await template_conn.execute(
            'select pg_terminate_backend(pid) '
            'from pg_stat_activity'
            " where datname = \'{name}\'".format(
                name=TEST_DB_NAME,
            ),
        )
        await template_conn.execute(
            'create database {name}_template template {name};'.format(
                name=TEST_DB_NAME,
            ),
        )

    try:
        yield
    finally:
        async with postgres_db as drop_conn:
            await drop_conn.execute(
                'drop database {name};'.format(name=TEST_DB_NAME),
            )
            await drop_conn.execute(
                'drop database {name}_template;'.format(name=TEST_DB_NAME),
            )


async def recreate_db() -> None:
    postgres_db = databases.Database(
        settings.CONN_TEMPLATE.format(
            user=settings.DB_USER,
            password=settings.DB_PASSWORD,
            port=settings.DB_PORT,
            host=settings.DB_SERVER,
            name='postgres',
        ),
    )
    async with postgres_db as drop_conn:
        await drop_conn.execute(
            'drop database {name} with (FORCE);'.format(
                name=TEST_DB_NAME,
            ),
        )
        await drop_conn.execute(
            'create database {name} template {name}_template;'.format(
                name=TEST_DB_NAME,
            ),
        )


@pytest_asyncio.fixture()
async def client():
    # run in transaction - makes tests faster
    # but it can cause problems in case of concurrency
    force_rollback = True

    database = databases.Database(
        settings.CONN_TEMPLATE.format(
            user=settings.DB_USER,
            password=settings.DB_PASSWORD,
            port=settings.DB_PORT,
            host=settings.DB_SERVER,
            name=TEST_DB_NAME,
        ),
        force_rollback=force_rollback,
    )
    async with database as db:
        async with AsyncClient(
            app=get_app(db=database, on_startup=[], on_shutdown=[]),
            base_url='http://test',
        ) as test_client:
            graphql_client = GraphQLTestClient(test_client, db)
            try:
                yield graphql_client
            except Exception:
                pass
    if not force_rollback:
        await recreate_db()
