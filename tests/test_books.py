import asyncio
import pytest
from src.books.models import CreateBookInput, create_book
from src.users.models import CreateUserInput, create_user
from tests.fixtures.clients import GraphQLTestClient
from tests.test_users import create_user_query
pytestmark = [
    pytest.mark.asyncio,
]


create_book_query = """
    mutation createBook(
        $input: CreateBookInput!
    ) {
        createBook(input: $input) {
            id
            user {
                id
            }
        }
    }
"""

books_query = """
    query books(
        $after: String
        $first: Int
    ) {
        books(
            after: $after
            first: $first
        ) {
            pageInfo {
                hasNextPage
                hasPreviousPage
                startCursor
                endCursor
            }
            edges {
                cursor
                node {
                    id
                }
            }
            nodes {
                id
            }
        }
    }
"""


async def test_create_book(
    client: GraphQLTestClient,
    mocker,
):
    create_user_resp = await client.query(
        query=create_user_query,
        variables={
            'input': {
                'name': 'Ayn Rand',
            },
        },
    )
    user_id = create_user_resp.data['createUser']['id']  # type: ignore
    resp = await client.query(
        query=create_book_query,
        variables={
            'input': {
                'title': 'Atlas shrugged',
                'userId': user_id,
            },
        },
    )
    assert resp.data is not None
    assert resp.data['createBook'] == {
        'id': mocker.ANY,
        'user': {
            'id': user_id,
        }
    }

async def test_get_books(
    client: GraphQLTestClient,
):
    user_inputs = [
        CreateUserInput(name='Ayn Rand'),
        CreateUserInput(name='Fyodor Dostoevsky'),
        CreateUserInput(name='J.K. Rowling'),
    ]
    user_1, user_2, user_3 = await asyncio.gather(
        *(create_user(
            db=client.db,
            create_input=user_input,
        ) for user_input in user_inputs)
    )
    book_inputs = [
        CreateBookInput(title='Atlas Shrugged', user_id=user_1.id),
        CreateBookInput(title='Anthem', user_id=user_1.id),
        CreateBookInput(title='Idiot', user_id=user_2.id),
        CreateBookInput(title='Demons', user_id=user_2.id),
        CreateBookInput(title='Crime and Punishment', user_id=user_2.id),
        CreateBookInput(
            title='Harry Potter and the Philosopher Stone',
            user_id=user_3.id
        ),
        CreateBookInput(
            title='Harry Potter and the Chamber of Secrets',
            user_id=user_3.id
        ),
        CreateBookInput(
            title='Harry Potter and the Prisoner of Azkaban',
            user_id=user_3.id
        ),
    ]
    _, book_2, book_3, book_4, book_5, *_ = await asyncio.gather(
        *(create_book(
            db=client.db,
            create_input=book_input,
        ) for book_input in book_inputs)
    )
    books_resp = await client.query(
        query=books_query,
        variables={
            'after': str(book_2.id),
            'first': 3,
        },
    )
    assert books_resp.data
    assert books_resp.data['books'] == {
        'pageInfo': {
            'hasNextPage': True,
            'hasPreviousPage': True,
            'startCursor': str(book_3.id),
            'endCursor': str(book_5.id),
        },
        'edges': [
            {
                'cursor': str(book_3.id),
                'node': {'id': book_3.id},
            },
            {
                'cursor': str(book_4.id),
                'node': {'id': book_4.id},
            },
            {
                'cursor': str(book_5.id),
                'node': {'id': book_5.id},
            }
        ],
        'nodes': [
            {'id': book_3.id},
            {'id': book_4.id},
            {'id': book_5.id},
        ],
    }
